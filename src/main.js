import { createApp, markRaw } from 'vue'
import { createPinia } from 'pinia'
import { QuillEditor } from '@vueup/vue-quill'
import '@vueup/vue-quill/dist/vue-quill.snow.css';
import '@vueup/vue-quill/dist/vue-quill.bubble.css';

import App from './App.vue'
import router from './router'

import './assets/styles/main.css'

const pinia = createPinia()
pinia.use(({ store }) => {
  store.router = markRaw(router)
})
const app = createApp(App)
app.component('QuillEditor', QuillEditor)
app.use(pinia)
app.use(router)

app.mount('#app')
