// import { collection, getDocs } from 'firebase/firestore'
import { collection, query, orderBy, limit, doc, onSnapshot, addDoc, deleteDoc, updateDoc } from 'firebase/firestore';
import { db } from '@/firebase'
import { defineStore } from 'pinia'
import { useAuthStore } from '@/stores/auth.js'

let dbConnector
let notesCollectionQuery
let getNotesSnapshot = null

export const useNotesStore = defineStore('notesStore', {
  state: () => {
    return {
      isNotesViewEmpty: true,
      createNote: false,
      readNote: false,
      isEditing: false,
      spinner: false,
      notes: [],
      singleNote: null,
      filteredNotes: []
    }
  },
  getters: {

  },
  actions: {
    init() {
      const authStore = useAuthStore()

      dbConnector = collection(db, 'users', authStore.user.id, 'musings')

      notesCollectionQuery = query(dbConnector, orderBy('date', 'desc'))
      this.loadDbData()
    },
    async addNewNote(newNote) {
      try {
        // Show a Spinner
        this.spinner = true

        // Add a new note to firebase db
        const docRef = await addDoc(dbConnector, {
          title: newNote.title,
          date: newNote.date,
          folder: newNote.folder,
          musing: newNote.musing,
          archived: false,
          favorite: false,
          deleted: false
        })

        this.spinner = false
        // TODO: Create and add notifications
      } catch (error) {
        console.log(error)
      }
    },

    // GET A Single note
    getNote(noteId) {
      const note = this.notes.find((note) => note.id === noteId)
      return this.singleNote = note
    },

    // DELETE a note
    async deleteNote(noteId) {
      this.spinner = true
      // On Delete, send document to trash
      // const deletedNote = doc(db, 'musings', noteId)

      // Set the 'deteted' field of the note 'true'
      await updateDoc(doc(dbConnector, noteId), {
        deleted: true
      })

      // await deleteDoc(doc(db, 'musings', noteId))
      // this.notes = this.notes.filter(notes => notes.id !== noteId)
      this.singleNote = null
      this.isNotesViewEmpty = true
      this.readNote = false
      this.spinner = false
      // TODO: Add notifcication on success
    },

    async updateExistingNote(noteId, updatedNote) {

      try {
        this.spinner = true
        await updateDoc(doc(dbConnector, noteId), {
          title: updatedNote.title,
          date: updatedNote.date,
          musing: updatedNote.musing,
          folder: updatedNote.folder
        })

        this.spinner = false
        this.isNotesViewEmpty = true,
          this.createNote = false,
          this.readNote = false,
          this.isEditing = false
      } catch (error) {
        console.log(error)
      }
    },
    loadDbData() {
      this.spinner = true

      try {

        getNotesSnapshot = onSnapshot(dbConnector, (querySnapshot) => {
          const tempNotes = []

          querySnapshot.forEach((doc) => {
            const note = {
              id: doc.id,
              title: doc.data().title,
              musing: doc.data().musing,
              folder: doc.data().folder,
              date: doc.data().date,
              deleted: doc.data().deleted,
              favorite: doc.data().favorite,
              archived: doc.data().archived,
            }
            tempNotes.push(note)
          })

          this.notes = tempNotes
          this.spinner = false
          // .filter((notes) => notes.deleted !== true)
        })
      } catch (error) {
        console.log(error)
      }
    },


    // Clear notes from DB
    clearNotes() {
      this.notes = []
      this.filteredNotes = []
      this.singleNote = {}
      // Unsubscribe from an active onSnapshot listener
      if (getNotesSnapshot) getNotesSnapshot()
    },


    // FAVORITE A NOTE
    async addToFavorites(noteId) {
      // Supdate the favorite field
      await updateDoc(doc(dbConnector, noteId), {
        favorite: true
      })
    },


    // ARCHIVE a note
    async addToArchived(noteId) {
      await updateDoc(doc(dbConnector, noteId), {
        archived: true
      })
    },


    // Filters for Eachfolder
    getPersonalNotes() {
      const notes = this.notes.filter((notes) => notes.deleted === false && notes.archived === false && notes.folder === 'Personal')
      this.filteredNotes = notes
    },
    getWorkNotes() {
      const notes = this.notes.filter((notes) => notes.deleted != true && notes.archived != true && notes.folder === 'Work')
      // console.log(notes)
      this.filteredNotes = notes
    },
    getTravelNotes() {
      const notes = this.notes.filter((notes) => notes.deleted != true && notes.archived != true && notes.folder === 'Travel')
      this.filteredNotes = notes
    },
    getEventsNotes() {
      const notes = this.notes.filter((notes) => notes.deleted != true && notes.archived != true && notes.folder === 'Events')
      this.filteredNotes = notes
    },
    getFinanceNotes() {
      const notes = this.notes.filter((notes) => notes.deleted != true && notes.archived != true && notes.folder === 'Finances')
      this.filteredNotes = notes
    },
    getDeletedNotes() {
      const notes = this.notes.filter((notes) => notes.deleted === true)
      this.filteredNotes = notes
    },
    getFavoriteNotes() {
      const notes = this.notes.filter((notes) => notes.favorite === true)
      this.filteredNotes = notes
    },
    getArchivedNotes() {
      const notes = this.notes.filter((notes) => notes.archived === true)
      this.filteredNotes = notes
    },
  }
})