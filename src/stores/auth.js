import { defineStore } from 'pinia'
import { auth } from '@/firebase/'
import { createUserWithEmailAndPassword, signOut, signInWithEmailAndPassword, onAuthStateChanged } from 'firebase/auth'
import { useNotesStore } from '@/stores/notes.js'


export const useAuthStore = defineStore('authStore', {
  state: () => {
    return {
      user: {}
    }
  },
  getters: {},
  actions: {
    init() {
      const storeNotes = useNotesStore()

      onAuthStateChanged(auth, (user) => {
        if (user) {
          this.user.id = user.uid
          this.user.email = user.email
          this.router.push('/')

          // load notes after user has been initialized
          storeNotes.init()
        } else {
          this.user = {}
          this.router.replace('/auth')
          storeNotes.clearNotes()
        }
      })
    },
    registerUser(credentials) {

      createUserWithEmailAndPassword(auth, credentials.email, credentials.password)
        .then((userCredential) => {
          const user = userCredential.user
          // this.user = user
        })
        .catch((error) => {
          const errorCode = error.code
          const errorMessage = error.message
        })
    },

    loginUser(credentials) {
      // console.log(credentials)

      signInWithEmailAndPassword(auth, credentials.email, credentials.password)
        .then((userCredential) => {
          const user = userCredential.user
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.log(`Error Message: ${error.message}`)
        })
    },

    signOutUser() {
      signOut(auth).then(() => {
        // Sign-out successful.
        // console.log('You have been signed out');
      }).catch((error) => {
        // An error happened.
        console.log('Error ' + error);
      })
    }
  }
})