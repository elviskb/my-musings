import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getAuth } from 'firebase/auth'


const firebaseConfig = {
  apiKey: 'AIzaSyDMAnQulvk4Oe8d1NCdxLupUFvQaRXKLx4',
  authDomain: 'my-musings-bcde9.firebaseapp.com',
  projectId: 'my-musings-bcde9',
  storageBucket: 'my-musings-bcde9.appspot.com',
  messagingSenderId: '354800899849',
  appId: '1:354800899849:web:7b48fffacc754adf80a5d8'
};

// Initialize Firebase
const app = initializeApp(firebaseConfig)
const db = getFirestore(app)
const auth = getAuth(app)

export {
  db,
  auth
}