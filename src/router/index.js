import { createRouter, createWebHistory } from 'vue-router'
import { useAuthStore } from "@/stores/auth.js";
import HomeView from '../views/HomeView.vue'
import AuthView from '../views/AuthView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/auth',
    name: 'auth',
    component: AuthView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

// Navigation guards
router.beforeEach(async (to, from) => {
  const authStore = useAuthStore()

  if (!authStore.user.id && to.name !== 'auth') {
    return { name: 'auth' }
  }

  if (authStore.user.id && to.name === 'auth') {
    return { name: 'home' }
  }
})

export default router
