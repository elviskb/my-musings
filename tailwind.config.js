/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'display': ['Inter'],
        'body': ['Inter'],
        'sans': ['Inter'],
        'title': ['Kaushan Script']
      }
    },
  },
  plugins: [],
}
